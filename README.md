# Finance Papers

This is an archive of my research papers in finance.


## Simple Portfolio Optimization That Works!

- [Paper](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2021simple-portfolio-optimization.pdf)

- [Data and Python Source-Code](https://gitlab.com/Hvass-Labs/FinanceOps/-/blob/master/Paper_Simple_Portfolio_Optimization.ipynb)

- [InvestOps Python package](https://gitlab.com/Hvass-Labs/InvestOps)

- [Video](https://youtu.be/5--5Ydtbu1Y)


## Fast Portfolio Diversification

- [Paper](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2022fast-portfolio-diversification.pdf)

- [Data and Python Source-Code](https://gitlab.com/Hvass-Labs/FinanceOps/-/blob/master/Paper_Fast_Portfolio_Diversification.ipynb)

- [InvestOps Python package](https://gitlab.com/Hvass-Labs/InvestOps)

- [Video](https://youtu.be/5--5Ydtbu1Y)


## Portfolio Group Constraints

- [Paper](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2022portfolio-group-constraints.pdf)

- [Data and Python Source-Code](https://gitlab.com/Hvass-Labs/FinanceOps/-/blob/master/Paper_Portfolio_Group_Constraints.ipynb)

- [InvestOps Python package](https://gitlab.com/Hvass-Labs/InvestOps)


## Long-Term Stock Forecasting

- [Paper](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2020long-term_stock_forecasting.pdf)

- [Data and Python Source-Code](https://gitlab.com/Hvass-Labs/FinanceOps/-/blob/master/Paper_Long-Term_Stock_Forecasting.ipynb)

- [InvestOps Python package](https://gitlab.com/Hvass-Labs/InvestOps)

- [Videos](https://www.youtube.com/playlist?list=PL9Hr9sNUjfsnzc1XrcXOl1PDLm6ESUAE1)


## Share Buyback Valuation

- [Full Treatise](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2012share-buyback.pdf)

- [Shorter Introduction](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2013share-buyback-intro.pdf)

- [Spreadsheet](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/share-buyback.xlsx)

- [Videos](https://www.youtube.com/playlist?list=PL9Hr9sNUjfsk2b8Y-MFU6W_rnCgwN51Ef)


## Does Volatility Harvesting Really Work?

- [Paper](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2021volatility-harvesting.pdf)

- [Data and Python Source-Code](https://gitlab.com/Hvass-Labs/FinanceOps/-/blob/master/Paper_Volatility_Harvesting.ipynb)

- [Video](https://youtu.be/t0AxhyQRRvM)


## S&P 500

- [Layman's Guide to Investing in the S&P 500](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2015layman-sp500.pdf)

- [Spreadsheet for Retirement Planning](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2015layman-sp500.xlsx)

- [Strategies for Investing in the S&P 500](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2015strategies-sp500.pdf)

- [Comparison of U.S. Stock Indices](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2015us-stock-comparison.pdf)


## Portfolio Optimization and Monte-Carlo Simulation

- [Paper](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2014portfolio-optimization.pdf)

- [Data and R Source-Code](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2014portfolio-optimization.zip)

- [Spreadsheet](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2014portfolio-optimization.xlsx)


## Monte Carlo Simulation in Financial Valuation

- [Paper](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2013monte-carlo.pdf)

- [Data and R Source-Code](https://gitlab.com/Hvass-Labs/Finance-Papers/-/raw/main/pedersen2013monte-carlo.zip)
